import { ISortingStrategy } from './ISortingStrategy';

// SELECTION SORT
export class BSort implements ISortingStrategy {
  public sort(arr: number[]): number[] {
    let minIdx;
    let temp;
    let length = arr.length;

    for (let i = 0; i < length; i++) {
      minIdx = i;

      for (let j = i + 1; j < length; j++) {
        if (arr[j] < arr[minIdx]) {
          minIdx = j;
        }
      }

      temp = arr[i];
      arr[i] = arr[minIdx];
      arr[minIdx] = temp;
    }

    return arr;
  }
}