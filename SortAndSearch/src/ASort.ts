import { ISortingStrategy } from './ISortingStrategy';

// BUBBLE SORT
export class ASort implements ISortingStrategy {
  public sort(arr: number[]): number[] {
    const length = arr.length;

    for (let i = length - 1; i >= 0; i--) {
      for (let j = 1; j <= i; j++) {
        if (arr[j - 1] > arr[j]) {
          const temp = arr[j - 1];
          arr[j - 1] = arr[j];
          arr[j] = temp;
        }
      }
    }

    return arr;
  }
}