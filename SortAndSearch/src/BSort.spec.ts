import { BSort } from './BSort';
import { unsorted, sorted } from './';

describe('Selection sort', () => {
  it('should sort array', () => {
    const sortingStrategy = new BSort();
    const sortedArray = sortingStrategy.sort(unsorted);
    expect(sortedArray).toEqual(sorted);
  });
})