import { ASort } from './ASort';
import { unsorted, sorted } from './';

describe('Bubble sort', () => {
  it('should sort array', () => {
    const sortingStrategy = new ASort();
    const sortedArray = sortingStrategy.sort(unsorted);
    expect(sortedArray).toEqual(sorted);
  });
})