export interface ISortingStrategy {
  sort: (arr: number[]) => number[];
}
