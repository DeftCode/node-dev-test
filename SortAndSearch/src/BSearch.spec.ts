import { sorted, elementsToFind } from './';
import { BSearch } from './BSearch';

describe('Binary search', () => {
  it('should find proper indexes', () => {
    const searchAlgorithm = new BSearch();
    const indexes = elementsToFind.map(element => searchAlgorithm.search(sorted, element));

    expect(indexes).toEqual([-1, 3, 6, -1, 12]);
    expect(searchAlgorithm.operationsCount).toEqual(3);
  });
});