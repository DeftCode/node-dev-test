export class BSearch {
  private operations: number = 0;

  public get operationsCount(): number {
    return this.operations;
  }

  public search(array: number[], target: number): number {
    this.operations = 0;
    let startIndex = 0;
    let endIndex = array.length - 1;

    while (startIndex <= endIndex) {
      this.operations = this.operations + 1;
      const middleIndex = Math.floor((startIndex + endIndex) / 2);

      if (target === array[middleIndex]) {
        return middleIndex
      }

      if (target > array[middleIndex]) {
        startIndex = middleIndex + 1;
      }

      if (target < array[middleIndex]) {
        endIndex = middleIndex - 1;
      }
    }

    return -1;
  }
}