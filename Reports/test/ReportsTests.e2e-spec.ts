import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/report/products/:date (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/products/2019%2F07%2F1')
      .expect(200)
      .expect([
        { productName: 'Black sport shoes', quantity: 2, totalPrice: 220 },
        { productName: 'Cotton t-shirt XL', quantity: 2, totalPrice: 51.5 },
      ]);
  });

  it('/report/customer/:date (GET)', () => {
    return request(app.getHttpServer())
      .get('/report/customer/2019%2F07%2F1')
      .expect(200)
      .expect([{ customerName: 'Jane Doe', totalPrice: 191.74}]);
  });
});
