import { Module } from '@nestjs/common';
import { ReportController } from './Controller/ReportController';
import { OrderModule } from '../Order/OrderModule';
import { ReportService } from './Service/ReportService';

@Module({
  imports: [OrderModule],
  controllers: [ReportController],
  providers: [ReportService],
})
export class ReportModule {}
