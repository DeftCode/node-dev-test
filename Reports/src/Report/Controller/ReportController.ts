import { Controller, Get, Param } from '@nestjs/common';
import { ReportService } from '../Service/ReportService';
import { IBestSeller, IBestBuyer } from '../Model/IReports';

@Controller('/report')
export class ReportController {
  constructor(private reportService: ReportService) {}

  @Get('/products/:date')
  public bestSellers(@Param('date') date: string): Promise<IBestSeller[]> {
    return this.reportService.getBestSellersByDate(date);
  }

  @Get('/customer/:date')
  public bestBuyers(@Param('date') date: string): Promise<IBestBuyer[]> {
    return this.reportService.getBestBuyersByDate(date);
  }
}
