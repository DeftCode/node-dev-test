import { Test } from '@nestjs/testing';
import { ReportService } from './ReportService';
import { OrderModule } from '../../Order/OrderModule';

describe('ReportService', () => {
  let reportService: ReportService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      imports: [OrderModule],
      providers: [ReportService],
    }).compile();

    reportService = module.get<ReportService>(ReportService);
  });

  it('should get best seller by given date', async () => {
    const expectedResult = [
      { productName: 'Black sport shoes', quantity: 2, totalPrice: 220 },
      { productName: 'Cotton t-shirt XL', quantity: 2, totalPrice: 51.5 },
    ];
    const bestSeller = await reportService.getBestSellersByDate('2019/07/1');

    expect(bestSeller).toEqual(expectedResult);
  });

  it('should get best buyer by given date', async () => {
    const expectedResult = [{ customerName: 'Jane Doe', totalPrice: 191.74}];
    const bestBuyer = await reportService.getBestBuyersByDate('2019/07/1');

    expect(bestBuyer).toEqual(expectedResult);
  });
});
