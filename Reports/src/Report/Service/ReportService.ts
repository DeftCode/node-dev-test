import { Injectable } from '@nestjs/common';
import { cloneDeep, flatten } from 'lodash';

import { IBestBuyer, IBestSeller } from '../Model/IReports';
import { OrderMapper } from '../../Order/Service/OrderMapper';
import { Order } from '../../Order/Model/Order';
import { Product } from '../../Order/Model/Product';

@Injectable()
export class ReportService {
  constructor(private orderMapper: OrderMapper) {}

  public async getBestBuyersByDate(date: string): Promise<IBestBuyer[]> {
    const orders = await this.orderMapper.getOrders(date);
    
    if (orders.length === 0) { return []; }

    const buyers = this.getBuyersFromOrders(orders);
    const sortedBuyers = this.sortObjectsByNumericValue(buyers, 'totalPrice');
    const bestBuyerTotalPrice = sortedBuyers[0].totalPrice;

    return sortedBuyers.filter(buyer => buyer.totalPrice === bestBuyerTotalPrice);
  }

  public async getBestSellersByDate(date: string): Promise<IBestSeller[]> {
    const orders = await this.orderMapper.getOrders(date);

    if (orders.length === 0) { return []; }
    
    const sellers = this.getSellersFromOrders(orders);
    const sortedSellers = this.sortObjectsByNumericValue(sellers, 'quantity');
    const bestSellerQuantity = sortedSellers[0].quantity;

    return sortedSellers.filter(seller => seller.quantity === bestSellerQuantity);
  }

  private getSellersFromOrders(orders: Order[]): IBestSeller[] {
    const products = flatten(orders.map(order => order.products));

    const sellers = products.reduce((container: {[key: number]: IBestSeller}, product: Product) => {
      const sellerID = product.id;

      if (!container[sellerID]) {
        container[sellerID] = { totalPrice: 0, productName: product.name, quantity: 0 };
      }

      container[sellerID].totalPrice = container[sellerID].totalPrice + product.price;
      container[sellerID].quantity = container[sellerID].quantity + 1;

      return container;
    }, {});

    return Object.values(sellers);
  }

  private sortObjectsByNumericValue<T>(objects: T[], property: string): T[] {
    const objectsCopy = cloneDeep(objects);
    return objectsCopy.sort((a: T, b: T) => {
      return b[property] - a[property];
    });
  }

  private getBuyersFromOrders(orders: Order[]): IBestBuyer[] {
    const buyers = orders.reduce((container: {[key: number]: IBestBuyer}, order: Order) => {
      const customerID = order.customer.id;

      if (!container[customerID]) {
        container[customerID] = { totalPrice: 0, customerName: order.customer.getFullName() };
      }

      container[customerID].totalPrice = container[customerID].totalPrice + this.getTotalProductsPrice(order.products);

      return container;
    }, {});

    return Object.values(buyers);
  }

  private getTotalProductsPrice(products: Product[]): number {
    return products.reduce((totalPrice: number, product: Product) => {
      return totalPrice + product.price;
    }, 0);
  }
}
