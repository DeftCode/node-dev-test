import { Customer } from './Customer';
import { Product } from './Product';

export class Order {
  public number: string;
  public customer: Customer;
  public products: Product[];

  constructor(data: Partial<Order> = {}) {
    this.number = data.number;
    this.customer = data.customer;
    this.products = data.products;
  }
}
