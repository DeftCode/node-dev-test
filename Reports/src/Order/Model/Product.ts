export class Product {
  public id: number;
  public name: string;
  public price: number;

  constructor(data: Partial<Product> = {}) {
    this.id = data.id;
    this.name = data.name;
    this.price = data.price;
  }
}
