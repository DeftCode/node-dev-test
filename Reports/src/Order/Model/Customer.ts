export class Customer {
  public id: number;
  public firstName: string;
  public lastName: string;

  constructor(data: Partial<Customer> = {}) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.lastName = data.lastName;
  }

  public getFullName(): string {
    return `${this.firstName} ${this.lastName}`;
  }
}
