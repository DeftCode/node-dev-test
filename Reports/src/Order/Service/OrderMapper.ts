import { Injectable, Inject } from '@nestjs/common';
import { Repository } from './Repository';
import { Order } from '../Model/Order';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';

@Injectable()
export class OrderMapper {
  @Inject() private repository: Repository;

  public async getOrders(date?: string): Promise<Order[]> {
    const customers = await this.getCustomers();
    const products = await this.getProducts();
    const orders = await this.getOrdersWithData(customers, products);

    return date ? orders.filter(order => order.number === date) : orders;
  }

  private async getOrdersWithData(customers: Customer[], products: Product[]): Promise<Order[]> {
    const orders = await this.repository.fetchOrders();

    return orders.map(order => {
      return new Order({
        ...order,
        customer: customers.find(cust => cust.id === order.customer),
        products: this.findOrderProducts(order.products, products),
      });
    });
  }

  private getCustomers(): Promise<Customer[]> {
    return this.repository.fetchCustomers().then(data => data.map(customer => new Customer(customer)));
  }

  private getProducts(): Promise<Product[]> {
    return this.repository.fetchProducts().then(data => data.map(product => new Product(product)));
  }

  private findOrderProducts(productIds: number[], products: Product[]): Product[] {
    return products.filter(product => productIds.some(id => id === product.id));
  }
}
