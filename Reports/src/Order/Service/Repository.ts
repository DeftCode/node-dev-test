import { Injectable } from '@nestjs/common';

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  public fetchOrders(): Promise<any[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  public fetchProducts(): Promise<any[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  public fetchCustomers(): Promise<any[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }
}
