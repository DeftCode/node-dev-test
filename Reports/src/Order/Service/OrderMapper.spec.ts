
import { Test } from '@nestjs/testing';
import { OrderMapper } from './OrderMapper';
import { Repository } from './Repository';
import { Customer } from '../Model/Customer';
import { Product } from '../Model/Product';
import { Order } from '../Model/Order';

const expectedResult = [
  new Order({
    customer: new Customer({ firstName: 'John', id: 1, lastName: 'Doe' }),
    number: '2019/07/1',
    products: [
      new Product({ id: 1, name: 'Black sport shoes', price: 110 }),
      new Product({ id: 2, name: 'Cotton t-shirt XL', price: 25.75 }),
    ],
  }),
  new Order({
    customer: new Customer({ firstName: 'Jane', id: 2, lastName: 'Doe' }),
    number: '2019/07/1',
    products: [
      new Product({ id: 1, name: 'Black sport shoes', price: 110 }),
      new Product({ id: 2, name: 'Cotton t-shirt XL', price: 25.75 }),
      new Product({ id: 3, name: 'Blue jeans', price: 55.99 }),
    ],
  }),
  new Order({
    customer: new Customer({ firstName: 'Jane', id: 2, lastName: 'Doe' }),
    number: '2019/07/2',
    products: [
      new Product({ id: 1, name: 'Black sport shoes', price: 110 }),
    ],
  }),
  new Order({
    customer: new Customer({ firstName: 'Jane', id: 2, lastName: 'Doe' }),
    number: '2019/08/1',
    products: [
      new Product({ id: 1, name: 'Black sport shoes', price: 110 }),
    ],
  }),
  new Order({
    customer: new Customer({ firstName: 'John', id: 1, lastName: 'Doe' }),
    number: '2019/08/2',
    products: [
      new Product({ id: 2, name: 'Cotton t-shirt XL', price: 25.75 }),
    ],
  }),
  new Order({
    customer: new Customer({ firstName: 'John', id: 1, lastName: 'Doe' }),
    number: '2019/08/3',
    products: [
      new Product({ id: 3, name: 'Blue jeans', price: 55.99 }),
    ],
  }),
];

describe('OrderMapper', () => {
  let orderMapper: OrderMapper;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [OrderMapper, Repository],
    }).compile();

    orderMapper = module.get<OrderMapper>(OrderMapper);
  });

  it('should return orders combined with customers and products', async () => {
    const orders = await orderMapper.getOrders();

    expect(orders).toEqual(expectedResult);
  });

  it('should return orders combined with customers and products filtered by date', async () => {
    const orders = await orderMapper.getOrders('2019/08/2');
    const expectedOrders = [{
      customer: { firstName: 'John', id: 1, lastName: 'Doe' },
      number: '2019/08/2',
      products: [
        { id: 2, name: 'Cotton t-shirt XL', price: 25.75 },
      ],
    }];

    expect(orders).toEqual(expectedOrders);
  });
});
